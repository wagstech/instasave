//
//  ProfileCollectionCell.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/12/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImv;

@property (weak, nonatomic) IBOutlet UILabel *postCountLb;

@property (weak, nonatomic) IBOutlet UILabel *followerCountLb;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLb;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLb;

@property (weak, nonatomic) IBOutlet UIButton *followBt;

- (IBAction)followAction:(id)sender;

@end
