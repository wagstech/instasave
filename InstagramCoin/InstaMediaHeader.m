//
//  InstaMediaHeader.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaMediaHeader.h"
#import "UIImageView+AFNetworking.h"

@implementation InstaMediaHeader

- (void) setData:(InstaMedia*)media{
    
    self.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 50);
    
    self.avatarImv.layer.cornerRadius = 15;
    self.avatarImv.clipsToBounds = YES;
    
    [self.avatarImv setImageWithURL:[NSURL URLWithString:media.user.profile_picture]];
    self.nameLb.text = media.user.username;
    self.likeLb.text = [NSString stringWithFormat:@"%d",(int)media.likeCount];
    self.commentLb.text = [NSString stringWithFormat:@"%d",(int)media.commentCount];
    
}

@end
