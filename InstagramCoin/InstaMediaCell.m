//
//  InstaMediaCell.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaMediaCell.h"

@implementation InstaMediaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)likeAcion:(id)sender {
//    [self.likeBt setImage:[UIImage imageNamed:@"ic_like_pink.png"] forState:UIControlStateNormal];
    [self.delegate didSelectLike:self];
}


- (IBAction)shareAction:(id)sender {
    [self.delegate didSelectShare:self];
}
@end
