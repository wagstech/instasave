//
//  InstaMediaHeader.h
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaMedia.h"

@interface InstaMediaHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *avatarImv;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *likeLb;
@property (weak, nonatomic) IBOutlet UILabel *commentLb;

- (void) setData:(InstaMedia*)media;


@end
