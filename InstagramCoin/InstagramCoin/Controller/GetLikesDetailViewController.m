//
//  GetLikesDetailViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "GetLikesDetailViewController.h"
#import "UIImageView+AFNetworking.h"

@interface GetLikesDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

- (IBAction)backAction:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *tbView;


@end

@implementation GetLikesDetailViewController{
    NSArray *arrayCoins;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayCoins = @[@"5",@"50",@"100",@"200",@"1000",@"2000",@"5000"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-  (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 0){
        UITableViewCell *cell = [_tbView dequeueReusableCellWithIdentifier:@"avatarCell"];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        [avatar setImageWithURL:[NSURL URLWithString:_media.photo_thumbnail]];
        avatar.layer.cornerRadius = 100;
        avatar.clipsToBounds = YES;
        avatar.layer.borderWidth = 2.0;
        avatar.layer.borderColor = [UIColor grayColor].CGColor;
        return cell;
    }
    
    UITableViewCell *cell = [_tbView dequeueReusableCellWithIdentifier:@"coinCell"];
    UIView *containView = [cell.contentView viewWithTag:333];
    containView.layer.cornerRadius = 10;
    containView.clipsToBounds = YES;
    
    UILabel *title = [cell.contentView viewWithTag:111];
    title.text = [NSString stringWithFormat:@"Get %@ likes",arrayCoins[indexPath.row -1]];
    
    UILabel *numberCoins = [cell.contentView viewWithTag:222];
    numberCoins.text = [NSString stringWithFormat:@"User %@",arrayCoins[indexPath.row -1]];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 200;
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_tbView deselectRowAtIndexPath:indexPath animated:YES];
}


- (IBAction)backAction:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
@end
