//
//  HomeViewController.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "HomeViewController.h"
#import "InstagramAPI.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"
#import "GetCoinsViewController.h"

@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@end

@implementation HomeViewController{
    CGFloat frameW;
    CGFloat frameH;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaMedias;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    frameH = [[UIScreen mainScreen] bounds].size.height;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
    [self getData];
 
}


- (void)refreshAction{

    [self getData];
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:0.3];
}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}

- (void) getData{
    [[InstagramAPI shareInstance] getFollowsCompletion:^(NSMutableArray *follows){
        
        instaMedias = [NSMutableArray array];
        [[InstagramAPI shareInstance] callAPIGetFeed:nil  completion:^(id data){
            
            for(id subData in data){
                if([subData isKindOfClass:[NSDictionary class]]){
                    InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                    [instaMedias addObject:media];
                }
            }
            
            [self sortFeedByTime];
            [self.collectionView reloadData];
        }];
        
        if(follows.count){
            for(InstaUser *user in follows){

                [[InstagramAPI shareInstance] callAPIGetFeed:user.userId  completion:^(id data){
                    
                    for(id subData in data){
                        if([subData isKindOfClass:[NSDictionary class]]){
                            InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                            [instaMedias addObject:media];
                        }
                    }
                    [self sortFeedByTime];
                    [self.collectionView reloadData];
                }];
            }
        }
    }];
}

- (void)sortFeedByTime{
    
    NSArray *arraySort = [instaMedias sortedArrayUsingComparator:^(id obj1, id obj2){
        InstaMedia *media1 = obj1;
        InstaMedia *media2 = obj2;
        
        if (media1.created_time < media2.created_time) {
            return (NSComparisonResult)NSOrderedDescending;
        } else if (media1.created_time  > media2.created_time) {
            return (NSComparisonResult)NSOrderedAscending ;
        }
        
        // TODO: default is the same?
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    instaMedias = [NSMutableArray arrayWithArray:arraySort];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return instaMedias.count > 13 ? instaMedias.count : 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(instaMedias.count > indexPath.row){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        InstaMedia *media = [instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    
    return emty;
    
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *index = [_collectionView indexPathForCell:sender];
    
    InstaMediaViewController *homeDetailVC = segue.destinationViewController;
    homeDetailVC.feeds = instaMedias;
    homeDetailVC.index = index.row;
    homeDetailVC.pageTitleStr = @"Feed";
 
}
- (IBAction)getCoinsAction:(id)sender {
    
    GetCoinsViewController *getCoinsVC = [[GetCoinsViewController alloc] init];
    CGRect frame = CGRectMake(0, frameH,frameW,frameH);
    getCoinsVC.view.frame = frame;
    
    [self.view addSubview:getCoinsVC.view];
    [self addChildViewController:getCoinsVC];
    
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        getCoinsVC.view.frame = frame;
    }];
    
    
}




@end
