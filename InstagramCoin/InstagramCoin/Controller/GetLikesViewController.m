//
//  GetLikesViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "GetLikesViewController.h"
#import "InstagramAPI.h"
#import "InstaUser.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"
#import "GetLikesDetailViewController.h"
#import "GetCoinsViewController.h"

@interface GetLikesViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

- (IBAction)getCoinAction:(id)sender;


@end

@implementation GetLikesViewController{
    CGFloat frameW;
    CGFloat frameH;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaMedias;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    frameH = [[UIScreen mainScreen] bounds].size.height;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
    
    [[InstagramAPI shareInstance] callAPIGetFeed:nil completion:^(id data){
        
        instaMedias = [NSMutableArray array];
        
        for(id subData in data){
            if([subData isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                [instaMedias addObject:media];
            }
        }
        
        [self.collectionView reloadData];
        
    }];
    
}

- (void)refreshAction{
    [[InstagramAPI shareInstance] callAPIGetFeed:nil completion:^(id data){
        
        [instaMedias removeAllObjects];
        
        for(id subData in data){
            if([subData isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                [instaMedias addObject:media];
            }
        }
         [self.collectionView.refreshControl endRefreshing];
        [self.collectionView reloadData];
        
    }];
    //[self performSelector:@selector(stopRefresh) withObject:nil afterDelay:0.3];
}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return instaMedias.count > 13 ? instaMedias.count : 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(instaMedias.count > indexPath.row){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        InstaMedia *media = [instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        
        UILabel *numberLike = [cell.contentView viewWithTag:222];
        numberLike.text = [NSString stringWithFormat:@"%d",(int)media.likeCount];
        
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    
    return emty;
    
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSInteger index = [_collectionView indexPathForCell:sender].row;
    GetLikesDetailViewController *vc = segue.destinationViewController;
    [vc setMedia:[instaMedias objectAtIndex:index]];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


- (IBAction)getCoinAction:(id)sender {
    GetCoinsViewController *getCoinsVC = [[GetCoinsViewController alloc] init];
    CGRect frame = CGRectMake(0, frameH,frameW,frameH);
    getCoinsVC.view.frame = frame;
    
    [self.view addSubview:getCoinsVC.view];
    [self addChildViewController:getCoinsVC];
    
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        getCoinsVC.view.frame = frame;
    }];
    
}
@end
