//
//  ExploreViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "ExploreViewController.h"
#import "InstagramAPI.h"
#import "InstaUser.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"
#import "SeachingViewController.h"
#include "ProfileViewController.h"
#import "InstaMediaCommonController.h"

@interface ExploreViewController ()<UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SearchingDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *searchContainerView;
@property (weak, nonatomic) IBOutlet UITextField *searchTf;
@property (weak, nonatomic) IBOutlet UILabel *searchLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchIconCenterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTraillingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelTraillingConstraint;

- (IBAction)cancelAction:(id)sender;


@end

@implementation ExploreViewController{
    CGFloat frameW;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaMedias;
    SeachingViewController *searchVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    _cancelTraillingConstraint.constant = - 80;
    _searchContainerView.layer.cornerRadius = 5;
    _searchContainerView.clipsToBounds = YES;
    self.searchTf.delegate = self;
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    
    
    self.collectionView.refreshControl = refreshControl;
    
    [[InstagramAPI shareInstance] getMediaNearByDistance:@"3" completion:^(NSMutableArray *data){
        
        instaMedias = [NSMutableArray array];
        
        for(id subData in data){
            if([subData isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                [instaMedias addObject:media];
            }
        }
        [self.collectionView reloadData];
    }];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    searchVC = [sb instantiateViewControllerWithIdentifier:@"SeachingViewController"];
    searchVC.delegate = self;
    
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenH = [[UIScreen mainScreen] bounds].size.height;
    
    CGRect desFrame = CGRectMake(0, 60 , screenW, screenH - 60);
    searchVC.view.frame = desFrame;
    searchVC.view.alpha = 0;
    
//    InstagramEngine *engine = [InstagramEngine sharedEngine];
//    [engine getPopularMediaWithSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
//        // media is an array of InstagramMedia objects
//      
//    } failure:^(NSError *error, NSInteger statusCode) {
//       
//    }];
    
    
    [self.view addSubview:searchVC.view];
    [self addChildViewController:searchVC];

}

- (void) didSelectUserResult:(InstaUser*)user{
    
    CGRect frame = CGRectMake(frameW, 0, frameW, [[UIScreen mainScreen] bounds].size.height - 60);
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *profileVC = [sb instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profileVC.view.frame = frame;
    profileVC.user = user;
    
    [self.view addSubview:profileVC.view];
    [self addChildViewController:profileVC];
    
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        profileVC.view.frame = frame;
    }];
    
}

- (void) didSelectHashTag:(InstaHashTag*)hashTah{
    
    [[InstagramAPI shareInstance] callAPIGetMediaByTag:hashTah.hashTag completion:^(id data){
        if(data == nil ){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Đây là tài khoản sanbox, không thể lấy được nội dung của tag này. Làm ơn chờ tới khi tài khoản công khai" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            NSMutableArray *result = [NSMutableArray array];
            for(id media in data){
                if([media isKindOfClass:[NSDictionary class]]){
                    InstaMedia *instaMedia = [[InstaMedia alloc] initWithData:media];
                    [result addObject:instaMedia];
                }
            }
            
            if(result.count == 0){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Đây là tài khoản sanbox, không thể lấy được nội dung của tag này. Làm ơn chờ tới khi tài khoản công khai" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            CGRect frame = CGRectMake(frameW, 0, frameW, [[UIScreen mainScreen] bounds].size.height - 60);
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            InstaMediaCommonController *mediaCommon = [sb instantiateViewControllerWithIdentifier:@"InstaMediaCommonController"];
            mediaCommon.view.frame = frame;
            mediaCommon.pageTitleStr = [NSString stringWithFormat:@"#%@",hashTah.hashTag];
            mediaCommon.instaMedias = result;
            
            [self.view addSubview:mediaCommon.view];
            [self addChildViewController:mediaCommon];
            
            frame.origin.x = 0;
            
            [UIView animateWithDuration:0.3 animations:^{
                mediaCommon.view.frame = frame;
            }];
            
        }
    }];

}


- (void)refreshAction{
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:0.3];
}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(_searchTf.text.length){
        return;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        _searchIconCenterConstraint.constant = 70 - self.searchContainerView.frame.size.width/2 ;
        _searchTraillingConstraint.constant = 80;
        _cancelTraillingConstraint.constant = 15;
        [self.view layoutIfNeeded];
    }completion:^(BOOL finish){
        _searchLb.hidden = YES;
        _searchTf.placeholder = @"Search by name or tag";
    }];

    searchVC.view.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        searchVC.view.alpha = 1.0;
    }];
 
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    

}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.searchTf resignFirstResponder];
     [searchVC startSearch:_searchTf.text];
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.searchTf resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return instaMedias.count > 13 ? instaMedias.count : 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(instaMedias.count > indexPath.row){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        InstaMedia *media = [instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        
        UILabel *numberLike = [cell.contentView viewWithTag:222];
        numberLike.text = [NSString stringWithFormat:@"%d",(int)media.likeCount];
        
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    
    return emty;
    
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [_collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"SegueCustom"]){
        NSIndexPath *index = [_collectionView indexPathForCell:sender];
        
        InstaMediaViewController *homeDetailVC = segue.destinationViewController;
        homeDetailVC.feeds = instaMedias;
        homeDetailVC.index = index.row;
        homeDetailVC.pageTitleStr = @"Explore";
    }
    
}

- (IBAction)cancelAction:(id)sender {
    self.searchTf.text = @"";
    [self.searchTf resignFirstResponder];
    [self searchingComplete];
    
    [UIView animateWithDuration:0.5 animations:^{
        searchVC.view.alpha = 0.0;
    } completion:^(BOOL finish){
        searchVC.view.hidden = YES;
    }];
}

- (void) searchingComplete{
    _searchTf.placeholder = @"";
    _searchLb.hidden = NO;

    [UIView animateWithDuration:0.5 animations:^{
        _searchIconCenterConstraint.constant = 0;
        _searchTraillingConstraint.constant = 20;
        _cancelTraillingConstraint.constant = - 80;
        [self.view layoutIfNeeded];
    }];
}

@end
