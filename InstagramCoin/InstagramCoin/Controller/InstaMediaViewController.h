//
//  InstaMediaViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstaMediaViewController : UIViewController


@property (strong,nonatomic) NSMutableArray *feeds;
@property (strong,nonatomic) NSString *pageTitleStr;
@property (assign,nonatomic) NSInteger index;

@end
