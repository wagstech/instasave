//
//  RepostViewController.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "RepostViewController.h"
#import "UIImageView+AFNetworking.h"
#import "WatermarkView.h"

#define myColor [UIColor colorWithRed:93.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]

typedef enum{
    BOTTOM,
    TOP,
    LEFT,
    RIGHT
}Position;

@interface RepostViewController ()

- (IBAction)backAcion:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *photoImv;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *containerView2;


@property (weak, nonatomic) IBOutlet UIButton *bottomBt;
@property (weak, nonatomic) IBOutlet UIButton *topBt;
@property (weak, nonatomic) IBOutlet UIButton *leftBt;
@property (weak, nonatomic) IBOutlet UIButton *rightBt;

@property (weak, nonatomic) IBOutlet UIButton *darkBt;
@property (weak, nonatomic) IBOutlet UIButton *lightBt;


- (IBAction)darkAction:(id)sender;
- (IBAction)lightAction:(id)sender;


- (IBAction)bottomAction:(id)sender;
- (IBAction)topAction:(id)sender;
- (IBAction)leftAction:(id)sender;
- (IBAction)rightAction:(id)sender;
- (IBAction)removeWaterMarkAction:(id)sender;
- (IBAction)repostAction:(id)sender;



@property (weak, nonatomic) IBOutlet UIView *watermarkContaintView;


@end

@implementation RepostViewController{
    WatermarkView *watermarkViewVer;
    WatermarkView *watermarkViewHoz;
    
    CGFloat frameW;
    Position waterPosition;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    frameW = [[UIScreen mainScreen] bounds].size.width;
    
    _containerView.layer.cornerRadius = 5;
    _containerView.clipsToBounds = YES;
    _containerView.layer.borderWidth = 1;
    _containerView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _containerView2.layer.cornerRadius = 5;
    _containerView2.clipsToBounds = YES;
    _containerView2.layer.borderWidth = 1;
    _containerView2.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.photoImv setImageWithURL:[NSURL URLWithString:_media.photo_standard_resolution]];
    
    
    watermarkViewVer = [[[NSBundle mainBundle] loadNibNamed:@"WatermarkView" owner:self options:nil] objectAtIndex:0];
    watermarkViewVer.transform = CGAffineTransformMakeRotation(M_PI_2);
    watermarkViewVer.frame = CGRectMake(-30, 0, 30, frameW);
    [watermarkViewVer setAutoresizingMask:UIViewAutoresizingNone];
    watermarkViewVer.appIconImv.layer.cornerRadius = 5;
    watermarkViewVer.clipsToBounds = YES;
    watermarkViewVer.avatarImv.layer.cornerRadius = 5;
    watermarkViewVer.avatarImv.clipsToBounds = YES;
    [watermarkViewVer.avatarImv setImageWithURL:[NSURL URLWithString:_media.user.profile_picture]];
    watermarkViewVer.nameLb.text = _media.user.username;
    [self.watermarkContaintView addSubview:watermarkViewVer];
    
    
    watermarkViewHoz = [[[NSBundle mainBundle] loadNibNamed:@"WatermarkView" owner:self options:nil] objectAtIndex:0];
    [watermarkViewHoz setAutoresizingMask:UIViewAutoresizingNone];
    watermarkViewHoz.frame = CGRectMake(0, frameW - 30, frameW, 30);;
    watermarkViewHoz.appIconImv.layer.cornerRadius = 5;
    watermarkViewHoz.clipsToBounds = YES;
    watermarkViewHoz.avatarImv.layer.cornerRadius = 5;
    watermarkViewHoz.avatarImv.clipsToBounds = YES;
    [watermarkViewHoz.avatarImv setImageWithURL:[NSURL URLWithString:_media.user.profile_picture]];
    watermarkViewHoz.nameLb.text = _media.user.username;
    [self.watermarkContaintView addSubview:watermarkViewHoz];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backAcion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)darkAction:(id)sender {
    self.darkBt.backgroundColor = [UIColor whiteColor];
    self.lightBt.backgroundColor = myColor;
    
    [self.darkBt setTitleColor:myColor forState:UIControlStateNormal];
    [self.lightBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    watermarkViewVer.blueView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    watermarkViewHoz.blueView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
}

- (IBAction)lightAction:(id)sender {
    self.lightBt.backgroundColor = [UIColor whiteColor];
    self.darkBt.backgroundColor = myColor;
    
    [self.lightBt setTitleColor:myColor forState:UIControlStateNormal];
    [self.darkBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    watermarkViewVer.blueView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    watermarkViewHoz.blueView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
}




- (IBAction)bottomAction:(id)sender {
    
    [self.bottomBt setImage:[UIImage imageNamed:@"ic_bottom_white.png"] forState:UIControlStateNormal];
    [self.topBt setImage:[UIImage imageNamed:@"ic_top.png"] forState:UIControlStateNormal];
    [self.leftBt setImage:[UIImage imageNamed:@"ic_left.png"] forState:UIControlStateNormal];
    [self.rightBt setImage:[UIImage imageNamed:@"ic_right.png"] forState:UIControlStateNormal];
    
    self.bottomBt.backgroundColor = [UIColor whiteColor];
    self.topBt.backgroundColor = myColor;
    self.leftBt.backgroundColor = myColor;
    self.rightBt.backgroundColor = myColor;
    
    if(waterPosition == BOTTOM){
        return;
    }
    
    [self hideWaterMark:BOTTOM];
    
}

- (IBAction)topAction:(id)sender {
    
    [self.bottomBt setImage:[UIImage imageNamed:@"ic_bottom.png"] forState:UIControlStateNormal];
    [self.topBt setImage:[UIImage imageNamed:@"ic_top_white.png"] forState:UIControlStateNormal];
    [self.leftBt setImage:[UIImage imageNamed:@"ic_left.png"] forState:UIControlStateNormal];
    [self.rightBt setImage:[UIImage imageNamed:@"ic_right.png"] forState:UIControlStateNormal];
    
    self.bottomBt.backgroundColor = myColor;
    self.topBt.backgroundColor = [UIColor whiteColor];
    self.leftBt.backgroundColor = myColor;
    self.rightBt.backgroundColor = myColor;
    
    if(waterPosition == TOP){
        return;
    }
    [self hideWaterMark:TOP];
    
}

- (IBAction)leftAction:(id)sender {
    
    [self.bottomBt setImage:[UIImage imageNamed:@"ic_bottom.png"] forState:UIControlStateNormal];
    [self.topBt setImage:[UIImage imageNamed:@"ic_top.png"] forState:UIControlStateNormal];
    [self.leftBt setImage:[UIImage imageNamed:@"ic_left_white.png"] forState:UIControlStateNormal];
    [self.rightBt setImage:[UIImage imageNamed:@"ic_right.png"] forState:UIControlStateNormal];
    
    self.bottomBt.backgroundColor = myColor;
    self.topBt.backgroundColor = myColor;
    self.leftBt.backgroundColor = [UIColor whiteColor];
    self.rightBt.backgroundColor = myColor;
    
    if(waterPosition == LEFT){
        return;
    }
    [self hideWaterMark:LEFT];
    
}

- (IBAction)rightAction:(id)sender {
    
    [self.bottomBt setImage:[UIImage imageNamed:@"ic_bottom.png"] forState:UIControlStateNormal];
    [self.topBt setImage:[UIImage imageNamed:@"ic_top.png"] forState:UIControlStateNormal];
    [self.leftBt setImage:[UIImage imageNamed:@"ic_left.png"] forState:UIControlStateNormal];
    [self.rightBt setImage:[UIImage imageNamed:@"ic_right_white.png"] forState:UIControlStateNormal];
    
    self.bottomBt.backgroundColor = myColor;
    self.topBt.backgroundColor = myColor;
    self.leftBt.backgroundColor = myColor;
    self.rightBt.backgroundColor = [UIColor whiteColor];
    
    if(waterPosition == RIGHT){
        return;
    }
    [self hideWaterMark:RIGHT];
    
}

- (IBAction)removeWaterMarkAction:(id)sender {
    
    [watermarkViewHoz removeFromSuperview];
    [watermarkViewVer removeFromSuperview];
    
}

- (IBAction)repostAction:(id)sender {
    
    UIImage *image = [self buildImage:_photoImv.image];
    NSArray *activityItems = @[image];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

- (void) hideWaterMark:(Position)newPosition{
    
    
    switch (waterPosition) {
        case BOTTOM:
        {
            CGRect newFrame = watermarkViewHoz.frame;
            newFrame.origin.y = frameW;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewHoz.frame = newFrame;
            } completion:^(BOOL finish){
                waterPosition = newPosition;
                [self showWaterMark];
            }];
            
        }
            break;
        case TOP:
        {
            CGRect newFrame = watermarkViewHoz.frame;
            newFrame.origin.y = -30;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewHoz.frame = newFrame;
            } completion:^(BOOL finish){
                waterPosition = newPosition;
                [self showWaterMark];
            }];
        }
            break;
        case LEFT:
        {
            CGRect newFrame = watermarkViewVer.frame;
            newFrame.origin.x = -30;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewVer.frame = newFrame;
            } completion:^(BOOL finish){
                waterPosition = newPosition;
                [self showWaterMark];
            }];
        }
            break;
        case RIGHT:
        {
            CGRect newFrame = watermarkViewVer.frame;
            newFrame.origin.x = frameW;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewVer.frame = newFrame;
            } completion:^(BOOL finish){
                waterPosition = newPosition;
                [self showWaterMark];
            }];
        }
            break;
        default:
            break;
    }
}

- (void) showWaterMark{
    
    switch (waterPosition) {
        case BOTTOM:
        {
            CGRect newFrame = watermarkViewHoz.frame;
            newFrame.origin.y = frameW;
            watermarkViewHoz.frame = newFrame;
            
            newFrame.origin.y = frameW - 30;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewHoz.frame = newFrame;
            }];
            
        }
            break;
        case TOP:
        {
            CGRect newFrame = watermarkViewHoz.frame;
            newFrame.origin.y = -30;
            watermarkViewHoz.frame = newFrame;
            
            newFrame.origin.y = 0;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewHoz.frame = newFrame;
            }];
        }
            break;
        case LEFT:
        {
            CGRect newFrame = watermarkViewVer.frame;
            newFrame.origin.x = -30;
            watermarkViewVer.frame = newFrame;
            
            newFrame.origin.x = 0;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewVer.frame = newFrame;
            }];
        }
            break;
        case RIGHT:
        {
            CGRect newFrame = watermarkViewVer.frame;
            newFrame.origin.x = frameW;
            watermarkViewVer.frame = newFrame;
            
            newFrame.origin.x = frameW-30;
            [UIView animateWithDuration:0.2 animations:^{
                watermarkViewVer.frame = newFrame;
            }];
            
        }
            break;
        default:
            break;
    }
}

- (UIImage*)buildImage:(UIImage*)image /*withSize:(CGSize)size*/
{
    
    NSLog(@"image size : %f-%f",image.size.width,image.size.height);

    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    
    [image drawAtPoint:CGPointZero];
    
    CGFloat scale = image.size.width / _watermarkContaintView.frame.size.width;
    //CGFloat scaleY = image.size.height / _workingView.frame.size.height;
    
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    [_watermarkContaintView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tmp;
}


@end
