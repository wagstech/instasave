//
//  RepostViewController.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaMedia.h"

@interface RepostViewController : UIViewController

@property (strong,nonatomic) InstaMedia *media;

@end
