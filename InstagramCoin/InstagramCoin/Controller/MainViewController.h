//
//  MainViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/10/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *homeView;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet UIView *likePlusView;

@property (weak, nonatomic) IBOutlet UIImageView *homeImv;
@property (weak, nonatomic) IBOutlet UIImageView *searchImv;
@property (weak, nonatomic) IBOutlet UIImageView *likeImv;
@property (weak, nonatomic) IBOutlet UIImageView *userImv;
@property (weak, nonatomic) IBOutlet UIImageView *likePlusImv;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *homeLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabbarLeadingConstraint;

@property (weak, nonatomic) IBOutlet UIView *tabbarView;
@property (strong,nonatomic) MenuViewController *menuVC;
@end
