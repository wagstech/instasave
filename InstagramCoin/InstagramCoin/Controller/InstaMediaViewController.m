//
//  InstaMediaViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaMediaViewController.h"
#import "InstaMediaHeader.h"
#import "InstaMedia.h"
#import "InstagramAPI.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaCell.h"
#import "RepostViewController.h"
#import "DGActivityIndicatorView.h"
#import "Utils.h"
#import <AVFoundation/AVFoundation.h>

@interface InstaMediaViewController ()<UITableViewDelegate,UITableViewDataSource,InstanMediaCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tbView;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;

@end

@implementation InstaMediaViewController{
    UIProgressView *progressView;
    DGActivityIndicatorView *activityIndicatorView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tbView.rowHeight = UITableViewAutomaticDimension;
    self.tbView.estimatedRowHeight = 300;
    _pageTitle.text = _pageTitleStr;
    
     activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallRotate tintColor:[UIColor whiteColor] size:20.0f];
    activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tbView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.index] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    CGRect frame = self.view.frame;
    frame.origin.x = [[UIScreen mainScreen] bounds].size.width;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }completion:^(BOOL finish){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    InstaMedia *media = [self.feeds objectAtIndex:section];
    
    InstaMediaHeader *header = [[[NSBundle mainBundle] loadNibNamed:@"InstaMediaHeader" owner:self options:nil] objectAtIndex:0];
    [header setData:media];
    
    return header;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.feeds.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    InstaMedia *media = [self.feeds objectAtIndex:indexPath.section];
    
    InstaMediaCell *cell;
    
    if(media.video_low_bandwidth){
        
        cell = [_tbView dequeueReusableCellWithIdentifier:@"InstaMediaVideoCell"];
        NSString *url = [[Utils shareInstance] getVideoPath:media.video_low_bandwidth];
        if(url){
            NSURL *videoURL = [NSURL fileURLWithPath:url];
            cell.avPlayer = [AVPlayer playerWithURL:videoURL];
            
            AVPlayerLayer* playerLayer = [AVPlayerLayer playerLayerWithPlayer:cell.avPlayer];
            playerLayer.frame = cell.containView.bounds;
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
            playerLayer.needsDisplayOnBoundsChange = YES;
            
            [cell.containView.layer addSublayer:playerLayer];
            cell.containView.layer.needsDisplayOnBoundsChange = YES;
            cell.photoImv.hidden = YES;
        }else{
            cell.photoImv.hidden = NO;
        }
        
    }else{
        cell = [_tbView dequeueReusableCellWithIdentifier:@"InstaMediaCell"];
    }
    
    if(media.user_has_liked){
        [cell.likeBt setImage:[UIImage imageNamed:@"ic_like_pink.png"] forState:UIControlStateNormal];
    }else{
        [cell.likeBt setImage:[UIImage imageNamed:@"ic_like.png"] forState:UIControlStateNormal];
    }
   
    cell.delegate = self;
    [cell.photoImv setImageWithURL:[NSURL URLWithString:media.photo_low_resolution]];
    cell.contentLb.text = media.captionStr;
    
    return cell;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSArray *cells = [_tbView visibleCells];
    
    for(InstaMediaCell *cell in cells){
        if(cell.avPlayer){
            [cell.avPlayer play];
        }
    }
    
}

- (void)didSelectLike:(id)cell{
    
    [activityIndicatorView startAnimating];
    
    NSIndexPath *index = [self.tbView indexPathForCell:cell];
    InstaMedia *media = [_feeds objectAtIndex:index.section];
    
    if(media.user_has_liked){
        [[InstagramAPI shareInstance] callAPIDislikeMedia:media.mediaId completion:^(BOOL success){
            [activityIndicatorView stopAnimating];
            if(success){
                media.user_has_liked = 0;
                [[(InstaMediaCell*)cell likeBt] setImage:[UIImage imageNamed:@"ic_like.png"] forState:UIControlStateNormal];
            }
        }];
    }else{
        [[InstagramAPI shareInstance] callAPILikeMedia:media.mediaId completion:^(BOOL success){
            [activityIndicatorView stopAnimating];
            if(success){
                media.user_has_liked = 1;
                [[(InstaMediaCell*)cell likeBt] setImage:[UIImage imageNamed:@"ic_like_pink.png"] forState:UIControlStateNormal];
            }
        }];
    }
    
}


- (void)didSelectShare:(id)cell{
    
    
    NSIndexPath *index = [self.tbView indexPathForCell:cell];
    InstaMedia *media = [_feeds objectAtIndex:index.section];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select an action" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
 
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
   
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Block User" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        

    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Copy link" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[UIPasteboard generalPasteboard] setString:media.link];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Copy success!" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Download this photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        [self performSelector:@selector(downloadImage) withObject:nil afterDelay:2.0];
        
    }]];

    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"View in Instagram" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSString *cut = [[media.link componentsSeparatedByString:@"/www.instagram.com/"] lastObject];
        NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://%@",cut]];
        NSURL *webinstagramURL = [NSURL URLWithString:media.link];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            [[UIApplication sharedApplication] openURL:instagramURL];
        }else{
            if ([[UIApplication sharedApplication] canOpenURL:webinstagramURL]) {
                [[UIApplication sharedApplication] openURL:webinstagramURL];
            }
        }
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void) downloadImage{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Download success!" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

//- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//    
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    id cell = [[[sender superview] superview] superview];
    NSIndexPath *index = [_tbView indexPathForCell:cell];
    
    RepostViewController *postVC = [segue destinationViewController];
    postVC.media = [_feeds objectAtIndex:index.section];
    
}

@end
