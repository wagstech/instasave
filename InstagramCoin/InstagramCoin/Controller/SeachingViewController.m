//
//  SeachingViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "SeachingViewController.h"
#import "InstagramAPI.h"
#import "InstaUser.h"
#import "InstaHashTag.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaCommonController.h"

@interface SeachingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLeadingConstraint;


@end

@implementation SeachingViewController{
    BOOL searchByName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrResult = [NSMutableArray array];
    searchByName = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) startSearch:(NSString*)keyword{
    if(searchByName){
        [[InstagramAPI shareInstance] callAPISearchUserByName:keyword completion:^(id data){
            [_arrResult removeAllObjects];
            
            for(id user in data){
                if([user isKindOfClass:[NSDictionary class]]){
                    InstaUser *instaUser = [[InstaUser alloc] initWithData:user];
                    [_arrResult addObject:instaUser];
                }
            }
            
            [self.tbView reloadData];

        }];
    }else{
        [[InstagramAPI shareInstance] callAPISearchUserByTag:keyword completion:^(id data){
            [_arrResult removeAllObjects];
            
            for(id result in data){
                if([result isKindOfClass:[NSDictionary class]]){
                    InstaHashTag *hashTag = [[InstaHashTag alloc] initWithData:result];
                    [_arrResult addObject:hashTag];
                }
            }
            
            [self.tbView reloadData];

        }];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrResult.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(searchByName){
        UITableViewCell *cell = [_tbView dequeueReusableCellWithIdentifier:@"cell"];
        UIImageView *avatar  = [cell.contentView viewWithTag:111];
        UILabel *nameLb  = [cell.contentView viewWithTag:222];
        
        avatar.layer.cornerRadius = avatar.frame.size.width/2;
        avatar.clipsToBounds = YES;
        
        InstaUser *user = [_arrResult objectAtIndex:indexPath.row];
        
        [avatar setImageWithURL:[NSURL URLWithString:user.profile_picture]];
        nameLb.text = user.username;
        return cell;
    }
    
    UITableViewCell *cell = [_tbView dequeueReusableCellWithIdentifier:@"hashTagCell"];
    UILabel *tag  = [cell.contentView viewWithTag:111];
    UILabel *numberPost  = [cell.contentView viewWithTag:222];
    
    
    InstaHashTag *hashTag = [_arrResult objectAtIndex:indexPath.row];
    tag.text = [NSString stringWithFormat:@"#%@",hashTag.hashTag];
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: hashTag.media_count]];
    
    numberPost.text = [NSString stringWithFormat:@"%@ posts",numberString];
   
    return cell;
    
}

- (IBAction)nameAction:(id)sender {
    
    searchByName = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.lineLeadingConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)tagAction:(id)sender {
    
    searchByName = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.lineLeadingConstraint.constant = [[UIScreen mainScreen] bounds].size.width/2;
        [self.view layoutIfNeeded];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tbView deselectRowAtIndexPath:indexPath animated:YES];
   
    if(searchByName){
        [self.delegate didSelectUserResult:[_arrResult objectAtIndex:indexPath.row]];
        return;
    }
    
    InstaHashTag *hashTah = [_arrResult objectAtIndex:indexPath.row];
    [self.delegate didSelectHashTag:hashTah];

}


@end
