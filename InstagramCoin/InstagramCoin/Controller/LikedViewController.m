//
//  LikedViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "LikedViewController.h"
#import "InstaMedia.h"
#import "InstagramAPI.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"

@interface LikedViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation LikedViewController{
    CGFloat frameW;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaLiked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
    
    [[InstagramAPI shareInstance] getLikedCompletion:^(NSMutableArray *likeds){
        
        instaLiked = [NSMutableArray array];
        
        for(id data in likeds){
            
            if([data isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:data];
                [instaLiked addObject:media];
            }
        }
        
        [self.collectionView reloadData];
    }];
    
}
- (void)refreshAction{
    
    [[InstagramAPI shareInstance] getLikedCompletion:^(NSMutableArray *likeds){
        
        [instaLiked removeAllObjects];
        [self.collectionView reloadData];
        
        for(id data in likeds){
            
            if([data isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:data];
                [instaLiked addObject:media];
            }
        }
         [self.collectionView.refreshControl endRefreshing];
        [self.collectionView reloadData];
    }];

}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return instaLiked.count > 13 ? instaLiked.count : 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(instaLiked.count > indexPath.row){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        InstaMedia *media = [instaLiked objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    
    return emty;
    
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *index = [_collectionView indexPathForCell:sender];
    
    InstaMediaViewController *homeDetailVC = segue.destinationViewController;
    homeDetailVC.feeds = instaLiked;
    homeDetailVC.index = index.row;
    homeDetailVC.pageTitleStr = @"Liked";
    
}

@end
