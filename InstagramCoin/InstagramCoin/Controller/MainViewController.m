//
//  MainViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/10/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "MainViewController.h"
#import "InstagramController.h"
#import "InstagramAPI.h"

//https://api.instagram.com/v1/users/self/?access_token=ACCESS-TOKEN
@interface MainViewController ()


@end

@implementation MainViewController{
    NSInteger currentTab;
    CGFloat frameW;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    frameW = [[UIScreen mainScreen] bounds].size.width;
    
    UITapGestureRecognizer *homeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homeAction)];
    [self.homeView addGestureRecognizer:homeTap];
    
    UITapGestureRecognizer *searchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchAction)];
    [self.searchView addGestureRecognizer:searchTap];
    
    UITapGestureRecognizer *likeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likeAction)];
    [self.likeView addGestureRecognizer:likeTap];
    
    UITapGestureRecognizer *userTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userAcion)];
    [self.userView addGestureRecognizer:userTap];
    
    UITapGestureRecognizer *likePTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likePlusAction)];
    [self.likePlusView addGestureRecognizer:likePTap];
    
    
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _menuVC = [sb instantiateViewControllerWithIdentifier:@"MenuViewController"];
    _menuVC.view.layer.masksToBounds = NO;
    _menuVC.view.layer.shadowOpacity = 1.f;
    _menuVC.view.layer.shadowOffset = CGSizeMake(0, 6);
    _menuVC.view.layer.shadowColor = [UIColor grayColor].CGColor;
    _menuVC.view.frame = CGRectMake(frameW, 0, frameW, [[UIScreen mainScreen] bounds].size.height);;
    [self.view addSubview:_menuVC.view];
    [self addChildViewController:_menuVC];
}

- (void) homeAction{
    if(currentTab == 0){
        return;
    }
    
    currentTab = 0;
    _homeLeadingConstraint.constant = 0;
    
    _homeImv.image = [UIImage imageNamed:@"ic_home_black.png"];
    _searchImv.image = [UIImage imageNamed:@"ic_search.png"];
    _likeImv.image = [UIImage imageNamed:@"ic_like.png"];
    _userImv.image = [UIImage imageNamed:@"ic_user.png"];
    _likePlusImv.image = [UIImage imageNamed:@"ic_like_plus.png"];
}

- (void) searchAction{
    if(currentTab == 1){
        return;
    }
    
    currentTab = 1;
    _homeLeadingConstraint.constant = -frameW;
    
    _homeImv.image = [UIImage imageNamed:@"ic_home.png"];
    _searchImv.image = [UIImage imageNamed:@"ic_search_black.png"];
    _likeImv.image = [UIImage imageNamed:@"ic_like.png"];
    _userImv.image = [UIImage imageNamed:@"ic_user.png"];
    _likePlusImv.image = [UIImage imageNamed:@"ic_like_plus.png"];
}


- (void) likeAction{
    if(currentTab == 2){
        return;
    }
    
    currentTab = 2;
    _homeLeadingConstraint.constant = -2*frameW;
    
    _homeImv.image = [UIImage imageNamed:@"ic_home.png"];
    _searchImv.image = [UIImage imageNamed:@"ic_search.png"];
    _likeImv.image = [UIImage imageNamed:@"ic_like_black.png"];
    _userImv.image = [UIImage imageNamed:@"ic_user.png"];
    _likePlusImv.image = [UIImage imageNamed:@"ic_like_plus.png"];
}


- (void) userAcion{
    if(currentTab == 3){
        return;
    }
    
    currentTab = 3;
    _homeLeadingConstraint.constant = -3*frameW;
    
    _homeImv.image = [UIImage imageNamed:@"ic_home.png"];
    _searchImv.image = [UIImage imageNamed:@"ic_search.png"];
    _likeImv.image = [UIImage imageNamed:@"ic_like.png"];
    _userImv.image = [UIImage imageNamed:@"ic_user_black.png"];
    _likePlusImv.image = [UIImage imageNamed:@"ic_like_plus.png"];
}


- (void) likePlusAction{
    if(currentTab == 4){
        return;
    }
    
    currentTab = 4;
    _homeLeadingConstraint.constant = -4*frameW;
    
    _homeImv.image = [UIImage imageNamed:@"ic_home.png"];
    _searchImv.image = [UIImage imageNamed:@"ic_search.png"];
    _likeImv.image = [UIImage imageNamed:@"ic_like.png"];
    _userImv.image = [UIImage imageNamed:@"ic_user.png"];
    _likePlusImv.image = [UIImage imageNamed:@"ic_like_plus_red.png"];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
