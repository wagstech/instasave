//
//  MenuViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "MenuViewController.h"
#import "ConstantHandle.h"
#import <MessageUI/MessageUI.h>
#import "GetCoinsViewController.h"

@interface MenuViewController ()<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>


@end

@implementation MenuViewController{
    NSArray *identifiers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    identifiers = @[@"cellGetCoins",@"cellFeedback",@"cellShareApp",@"cellLogout"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [_tbView dequeueReusableCellWithIdentifier:identifiers[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tbView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            [self getCoins];
            break;
        case 1:
            [self sendFeedback];
            break;
        case 2:
            [self shareApp];
            break;
        case 3:
            [self logout];
            break;
        default:
            break;
    }
    
}

- (void) getCoins{
    GetCoinsViewController *getCoinsVC = [[GetCoinsViewController alloc] init];
    
    CGFloat frameW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat frameH = [[UIScreen mainScreen] bounds].size.height;
    
    CGRect frame = CGRectMake(0, frameH,frameW,frameH);
    getCoinsVC.view.frame = frame;
    
    UIViewController *vc = [self.navigationController.viewControllers lastObject];
    
    [vc.view addSubview:getCoinsVC.view];
    [vc addChildViewController:getCoinsVC];
    
    frame.origin.y = 0;
    [UIView animateWithDuration:0.3 animations:^{
        getCoinsVC.view.frame = frame;
    }];

}

- (void)sendFeedback {
    
    if([MFMailComposeViewController canSendMail]){
        NSString *emailTitle = @"Test Email";
        // Email Content
        NSString *messageBody = @"Test Subject!";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@test.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) logout{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:INSTAGRAM_ACCESS_TOKEN];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) shareApp{
    
    NSString* text = @"let download this app";
    NSArray *activityItems = @[text];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}


@end
