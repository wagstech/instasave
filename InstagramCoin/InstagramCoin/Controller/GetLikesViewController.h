//
//  GetLikesViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetLikesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
