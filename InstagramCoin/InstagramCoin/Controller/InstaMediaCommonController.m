//
//  InstaMediaCommonController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaMediaCommonController.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMedia.h"
#import "InstaMediaViewController.h"

@interface InstaMediaCommonController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
- (IBAction)backAction:(id)sender;
@end

@implementation InstaMediaCommonController{
    CGFloat frameW;
    NSInteger numberColums;
    CGFloat itemSpace;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    frameW = [[UIScreen mainScreen] bounds].size.width;
    numberColums = 3;
    itemSpace = 2;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    self.pageTitle.text = _pageTitleStr;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *index = [_collectionView indexPathForCell:sender];
    InstaMediaViewController *mediaVC = segue.destinationViewController;
    mediaVC.index = index.row;
    mediaVC.feeds = _instaMedias;
    mediaVC.pageTitleStr = _pageTitleStr;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _instaMedias.count > 13 ? _instaMedias.count : 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_instaMedias.count > indexPath.row){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        InstaMedia *media = [_instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    
    return emty;
    
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}


- (IBAction)backAction:(id)sender {
    
    CGRect frame = self.view.frame;
    frame.origin.x = [[UIScreen mainScreen] bounds].size.width;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }completion:^(BOOL finish){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
@end
