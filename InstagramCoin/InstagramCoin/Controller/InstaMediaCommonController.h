//
//  InstaMediaCommonController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstaMediaCommonController : UIViewController

@property (strong,nonatomic) NSMutableArray *instaMedias;
@property (strong,nonatomic) NSString *pageTitleStr;


@end
