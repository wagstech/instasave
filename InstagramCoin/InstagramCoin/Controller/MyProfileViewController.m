//
//  MyProfileViewController.m
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ProfileCollectionCell.h"
#import "InstagramAPI.h"
#import "InstaUser.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"
#import "MenuViewController.h"
#import "MainViewController.h"

@interface MyProfileViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *userName;

- (IBAction)menuAction:(id)sender;

@end

@implementation MyProfileViewController{
    
    CGFloat frameW;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaMedias;
    InstaUser *user;
    ProfileCollectionCell *inforCell;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(leftSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.collectionView addGestureRecognizer:recognizer];
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                           action:@selector(rightSwipe)];
    //recognizer.delegate = self;
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.collectionView addGestureRecognizer:recognizer];
    
}

- (void) rightSwipe{
    
    MainViewController *mainVC = [self.navigationController.viewControllers lastObject];
    
    if(mainVC.tabbarLeadingConstraint.constant != 0){
        [self menuAction:nil];
    }
}

- (void) leftSwipe{
    MainViewController *mainVC = [self.navigationController.viewControllers lastObject];
    
    if(mainVC.tabbarLeadingConstraint.constant == 0){
        [self menuAction:nil];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
    
    [[InstagramAPI shareInstance] callAPIGetUser:nil completion:^(id data){
        
        user = [[InstaUser alloc] initWithData:data];
        self.userName.text = user.username;
        [self.collectionView reloadData];
        
    }];
    
    [[InstagramAPI shareInstance] callAPIGetFeed:nil completion:^(id data){
        
        instaMedias = [NSMutableArray array];
        
        for(id subData in data){
            if([subData isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                [instaMedias addObject:media];
            }
        }
        [self.collectionView reloadData];
        
    }];
    
}
- (void)refreshAction{
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:0.3];
}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}


- (IBAction)settingAction:(id)sender {
    
}


- (IBAction)refreshAction:(id)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    
    return instaMedias.count > 10 ? instaMedias.count : 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        inforCell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"inforCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        inforCell.avatarImv.layer.cornerRadius = 35;
        inforCell.avatarImv.clipsToBounds = YES;
        inforCell.fullNameLb.text = user.full_name;
        [inforCell.avatarImv setImageWithURL:[NSURL URLWithString:user.profile_picture]];
        inforCell.followingCountLb.text = [NSString stringWithFormat:@"%d",(int)user.follows];
        inforCell.followerCountLb.text = [NSString stringWithFormat:@"%d",(int)user.followed_by];
        inforCell.postCountLb.text = [NSString stringWithFormat:@"%d",(int)user.media];
        return inforCell;
    }
    
    if(instaMedias.count > indexPath.row ){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"mediaCell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        
        InstaMedia *media = [instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    if(emty == nil){
        emty = [[UICollectionViewCell alloc] init];
    }
    return emty;
    
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0){
        return CGSizeMake(frameW, 150);
    }
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *index = [_collectionView indexPathForCell:sender];
    
    InstaMediaViewController *homeDetailVC = segue.destinationViewController;
    homeDetailVC.feeds = instaMedias;
    homeDetailVC.index = index.row;
    homeDetailVC.pageTitleStr = _userName.text;
    
}



- (IBAction)menuAction:(id)sender {
    
    MainViewController *mainVC = [self.navigationController.viewControllers lastObject];
    
    if(mainVC.tabbarLeadingConstraint.constant == 0){
        CGRect menuFrame = mainVC.menuVC.view.frame;
        menuFrame.origin.x = 100;
        
        self.collectionView.userInteractionEnabled = NO;
        mainVC.tabbarView.userInteractionEnabled = NO;
        
        CGFloat newMainVCConstraint = mainVC.homeLeadingConstraint.constant - frameW  + 100;
        CGFloat newTabbarCConstraint = 100 - frameW;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            mainVC.menuVC.view.frame = menuFrame;
            mainVC.homeLeadingConstraint.constant = newMainVCConstraint;
            mainVC.tabbarLeadingConstraint.constant = newTabbarCConstraint;
            [mainVC.view layoutIfNeeded ];
        }];
        
    }else{
        
        self.collectionView.userInteractionEnabled = YES;
        mainVC.tabbarView.userInteractionEnabled = YES;
        
        CGRect menuFrame = mainVC.menuVC.view.frame;
        menuFrame.origin.x = frameW;
        
        CGFloat newMainVCConstraint = mainVC.homeLeadingConstraint.constant + frameW  - 100;
        CGFloat newTabbarCConstraint = 0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            mainVC.menuVC.view.frame = menuFrame;
            mainVC.homeLeadingConstraint.constant = newMainVCConstraint;
            mainVC.tabbarLeadingConstraint.constant = newTabbarCConstraint;
            [mainVC.view layoutIfNeeded ];
        }];
    }
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self menuAction:nil];
}



@end
