//
//  GetCoinsViewController.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetCoinsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *numberCoinsLb;

@end
