//
//  ProfileViewController.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaUser.h"

@interface ProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) InstaUser *user;
- (IBAction)backAction:(id)sender;

@end
