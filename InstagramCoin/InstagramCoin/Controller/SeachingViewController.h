//
//  SeachingViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaUser.h"
#import "InstaHashTag.h"

@protocol SearchingDelegate <NSObject>

- (void) didSelectUserResult:(InstaUser*)user;
- (void) didSelectHashTag:(InstaHashTag*)tag;

@end

@interface SeachingViewController : UIViewController

@property(weak,nonatomic) id<SearchingDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tbView;
@property (strong,nonatomic) NSMutableArray *arrResult;

- (IBAction)nameAction:(id)sender;
- (IBAction)tagAction:(id)sender;

- (void) startSearch:(NSString*)keyword;


@end
