//
//  ProfileViewController.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileCollectionCell.h"
#import "InstagramAPI.h"
#import "InstaUser.h"
#import "InstaMedia.h"
#import "UIImageView+AFNetworking.h"
#import "InstaMediaViewController.h"
#import "MenuViewController.h"
#import "MainViewController.h"

@interface ProfileViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *userName;


@end

@implementation ProfileViewController{
    
    CGFloat frameW;
    NSInteger numberColums;
    CGFloat itemSpace;
    NSMutableArray *instaMedias;
    ProfileCollectionCell *inforCell;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    numberColums = 3;
    itemSpace = 2.0;
    frameW = [[UIScreen mainScreen] bounds].size.width;
    
    
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self
                       action:@selector(refreshAction)
             forControlEvents:UIControlEventValueChanged];
    self.collectionView.refreshControl = refreshControl;
    
    
    [[InstagramAPI shareInstance] callAPIGetUser:_user.userId completion:^(id data){
        _user = nil;
        _user = [[InstaUser alloc] initWithData:data];
        self.userName.text = _user.username;
        [self.collectionView reloadData];
        
    }];
    
    
    [[InstagramAPI shareInstance] callAPIGetFeed:_user.userId completion:^(id data){
        instaMedias = [NSMutableArray array];
        
        for(id subData in data){
            if([subData isKindOfClass:[NSDictionary class]]){
                InstaMedia *media = [[InstaMedia alloc] initWithData:subData];
                [instaMedias addObject:media];
            }
        }
        [self.collectionView reloadData];
        
    }];
    
}
- (void)refreshAction{
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:0.3];
}

- (void) stopRefresh{
    [self.collectionView.refreshControl endRefreshing];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    
    return instaMedias.count > 10 ? instaMedias.count : 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        inforCell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"inforCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        inforCell.avatarImv.layer.cornerRadius = 35;
        inforCell.avatarImv.clipsToBounds = YES;
        inforCell.fullNameLb.text = _user.full_name;
        [inforCell.avatarImv setImageWithURL:[NSURL URLWithString:_user.profile_picture]];
        inforCell.followingCountLb.text = [NSString stringWithFormat:@"%d",(int)_user.follows];
        inforCell.followerCountLb.text = [NSString stringWithFormat:@"%d",(int)_user.followed_by];
        inforCell.postCountLb.text = [NSString stringWithFormat:@"%d",(int)_user.media];
        inforCell.followBt.layer.cornerRadius = 10;
        inforCell.followBt.clipsToBounds = YES;
        inforCell.followBt.layer.borderWidth = 1.0;
        inforCell.followBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
        return inforCell;
    }
    
    if(instaMedias.count > indexPath.row ){
        UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"mediaCell" forIndexPath:indexPath];
        UIImageView *avatar = [cell.contentView viewWithTag:111];
        
        InstaMedia *media = [instaMedias objectAtIndex:indexPath.row];
        [avatar setImageWithURL:[NSURL URLWithString:media.photo_thumbnail]];
        return cell;
    }
    
    UICollectionViewCell *emty = [_collectionView dequeueReusableCellWithReuseIdentifier:@"emty" forIndexPath:indexPath];
    if(emty == nil){
        emty = [[UICollectionViewCell alloc] init];
    }
    return emty;
    
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

// set size items
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0){
        return CGSizeMake(frameW, 150);
    }
    
    CGFloat width = (frameW - (numberColums-1)*itemSpace)/numberColums;
    return CGSizeMake(width, width);
}


// set space bettwen 2 items in the line
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

// set space bettwen 2 items in 2 lines
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return itemSpace;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *index = [_collectionView indexPathForCell:sender];
    
    InstaMediaViewController *homeDetailVC = segue.destinationViewController;
    homeDetailVC.feeds = instaMedias;
    homeDetailVC.index = index.row;
    homeDetailVC.pageTitleStr = _userName.text;
}

- (IBAction)backAction:(id)sender {
    
    CGRect frame = self.view.frame;
    frame.origin.x = frameW;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finish){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}
@end
