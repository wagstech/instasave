//
//  GetCoinsViewController.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "GetCoinsViewController.h"

@interface GetCoinsViewController ()

- (IBAction)closeAction:(id)sender;
- (IBAction)storeAction:(id)sender;
- (IBAction)removeMarkAction:(id)sender;
- (IBAction)buy10CoinsAcion:(id)sender;
- (IBAction)buy50CoinsAcion:(id)sender;
- (IBAction)buy250CoinsAcion:(id)sender;
- (IBAction)buy100CoinsAcion:(id)sender;
- (IBAction)buyProVersion:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *functionBt;
@property (weak, nonatomic) IBOutlet UIButton *functionBt1;
@property (weak, nonatomic) IBOutlet UIButton *functionBt2;

@property (weak, nonatomic) IBOutlet UIButton *functionBt3;
@property (weak, nonatomic) IBOutlet UIButton *functionBt4;
@property (weak, nonatomic) IBOutlet UIButton *functionBt5;

@end

@implementation GetCoinsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _functionBt.layer.cornerRadius = 10;
    _functionBt.clipsToBounds = YES;
    
    _functionBt1.layer.cornerRadius = 10;
    _functionBt1.clipsToBounds = YES;
    
    _functionBt2.layer.cornerRadius = 10;
    _functionBt2.clipsToBounds = YES;
    
    _functionBt3.layer.cornerRadius = 10;
    _functionBt3.clipsToBounds = YES;
    
    _functionBt4.layer.cornerRadius = 10;
    _functionBt4.clipsToBounds = YES;
    
    _functionBt5.layer.cornerRadius = 10;
    _functionBt5.clipsToBounds = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)closeAction:(id)sender {
    
    CGRect frame = self.view.frame;
    frame.origin.y = [[UIScreen mainScreen] bounds].size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
        
    }completion:^(BOOL finish){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

- (IBAction)storeAction:(id)sender {
}

- (IBAction)removeMarkAction:(id)sender {
}

- (IBAction)buy10CoinsAcion:(id)sender {
}

- (IBAction)buy50CoinsAcion:(id)sender {
}

- (IBAction)buy250CoinsAcion:(id)sender {
}
- (IBAction)buy100CoinsAcion:(id)sender {
}

- (IBAction)buyProVersion:(id)sender {
}
@end
