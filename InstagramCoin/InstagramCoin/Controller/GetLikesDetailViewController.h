//
//  GetLikesDetailViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstaMedia.h"

@interface GetLikesDetailViewController : UIViewController

@property (strong,nonatomic) InstaMedia *media;

@end
