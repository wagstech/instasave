//
//  MyProfileViewController.h
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
