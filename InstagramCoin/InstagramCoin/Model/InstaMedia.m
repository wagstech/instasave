//
//  InstaMedia.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaMedia.h"
#import "Utils.h"

@implementation InstaMedia

- (id) initWithData:(NSDictionary*)data{
    
    self.attribution = data[@"attribution"];
    self.caption = data[@"caption"];
    if([_caption isKindOfClass:[NSDictionary class]]){
        self.captionStr = self.caption[@"text"];
    }
    
    id comments = data[@"comments"];
    if([comments isKindOfClass:[NSDictionary class]]){
        self.commentCount = [comments[@"count"] integerValue];
    }
    
    self.created_time = [data[@"created_time"] doubleValue];
    self.filter = data[@"filter"];
    self.mediaId = data[@"id"];
    
    id images = data[@"images"];
    if([images isKindOfClass:[NSDictionary class]]){
        self.photo_low_resolution = images[@"low_resolution"][@"url"];
        self.photo_standard_resolution = images[@"standard_resolution"][@"url"];
        self.photo_thumbnail = images[@"thumbnail"][@"url"];
    }
    
    id videos = data[@"videos"];
    if([videos isKindOfClass:[NSDictionary class]]){
        
        self.video_low_bandwidth = videos[@"low_bandwidth"][@"url"];
        self.video_low_resolution = videos[@"low_resolution"][@"url"];
        self.video_standard_resolution = videos[@"standard_resolution"][@"url"];
        
        [[Utils shareInstance] downloadVideo:self.video_low_bandwidth completion:nil];        
    }
    
    
    id likes = data[@"likes"];
    if([likes isKindOfClass:[NSDictionary class]]){
        self.likeCount = [likes[@"count"] integerValue];
    }
    
    self.link = data[@"link"];
    self.location = data[@"location"];
    self.tags = data[@"tags"];
    
    id user = data[@"user"];
    if([user isKindOfClass:[NSDictionary class]]){
        self.user = [[InstaUser alloc] initWithData:user];
    }
    
    self.user_has_liked = [data[@"user_has_liked"] integerValue];
    self.users_in_photo = data[@"users_in_photo"];
    
    return self;
}

@end
