//
//  InstaHashTag.m
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaHashTag.h"

@implementation InstaHashTag

- (id) initWithData:(NSDictionary*)data{
    self.media_count = [data[@"media_count"] integerValue];
    self.hashTag = data[@"name"];
    
    return self;
}

@end
