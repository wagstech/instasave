//
//  InstagramAPI.h
//  InstagramCoin
//
//  Created by Dreamup on 2/10/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramAPI : NSObject

+ (id) shareInstance;

- (void) callAPIGetFeed:(NSString*)path completion:(void(^)(id))completion;
- (void) callAPIGetUser:(NSString*)path completion:(void(^)(id))completion;

- (void) callAPISearchUserByName:(NSString*)name completion:(void(^)(id))completion;
- (void) callAPISearchUserByTag:(NSString*)name completion:(void(^)(id))completion;
- (void) callAPIGetMediaByTag:(NSString*)tag completion:(void(^)(id))completion;

- (void)callAPILikeMedia:(NSString *)mediaId completion:(void(^)(BOOL success))completion;
- (void)callAPIDislikeMedia:(NSString *)mediaId completion:(void (^)(BOOL success))completion;

- (void) getFollowsCompletion:(void(^)(NSMutableArray*))completion;
- (void) getFollowedCompletion:(void(^)(NSMutableArray*))completion;
- (void) getLikedCompletion:(void(^)(NSMutableArray*))completion;
- (void) getMediaNearByDistance:(NSString*)distance completion:(void(^)(NSMutableArray*))completion;

@end
