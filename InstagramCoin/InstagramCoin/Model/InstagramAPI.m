//
//  InstagramAPI.m
//  InstagramCoin
//
//  Created by Dreamup on 2/10/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstagramAPI.h"
#import "AFNetworking.h"
#import "ConstantHandle.h"
#import "InstaUser.h"


static InstagramAPI *instagramAI = nil;

@implementation InstagramAPI{
    
}


+ (id) shareInstance{
    if(instagramAI == nil){
        instagramAI = [[InstagramAPI alloc] init];
    }
    return instagramAI;
}




- (void) callAPIGetFeed:(NSString*)userID completion:(void(^)(id))completion{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = userID == nil ?
    [NSString stringWithFormat:@"users/self/media/recent/?access_token=%@",token] : [NSString stringWithFormat:@"users/%@/media/recent/?access_token=%@",userID,token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            if(data){
                completion(data);
            }
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
}

- (void) callAPIGetUser:(NSString*)userID completion:(void(^)(id))completion{
    
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = userID == nil ?
    [NSString stringWithFormat:@"users/self/?access_token=%@",token] : [NSString stringWithFormat:@"users/%@/?access_token=%@",userID,token];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            if(data){
                completion(data);
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
}

- (void) callAPIGetFollowsCompletion:(void(^)(id))completion{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path =
    [NSString stringWithFormat:@"users/self/follows?access_token=%@",token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            completion(data);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
}


- (void) callAPISearchUserByName:(NSString*)name completion:(void(^)(id))completion{
    //users/search?q=jack&access_token=ACCESS-TOKEN
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"users/search?q=%@&access_token=%@",name,token];
     path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            completion(data);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
    
    
}

- (void) callAPIGetMediaByTag:(NSString*)tag completion:(void(^)(id))completion{
    
    //NSString *tagEncoded = [tag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"tags/%@/media/recent?access_token=%@",tag,token];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        id data = [responseObject objectForKey:@"data"];
        if([data isKindOfClass:[NSArray class]]){
            completion(data);
        }else completion(nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
}

- (void) callAPISearchUserByTag:(NSString*)name completion:(void(^)(id))completion{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"tags/search?q=%@&access_token=%@",name,token];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            completion(data);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
}


- (void)callAPILikeMedia:(NSString *)mediaId completion:(void (^)(BOOL success))completion{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"media/%@/likes?access_token=%@",mediaId,token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id meta = [responseObject objectForKey:@"meta"];
            if([meta isKindOfClass:[NSDictionary class]]){
                NSInteger code = [meta[@"code"] integerValue];
                if(code == 200){
                    completion(YES);
                }else completion(NO);
            }
            
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(NO);
        NSLog(@"error : %@",error);
    }];
    
}

- (void)callAPIDislikeMedia:(NSString *)mediaId completion:(void (^)(BOOL success))completion{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"media/%@/likes?access_token=%@",mediaId,token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager DELETE:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id meta = [responseObject objectForKey:@"meta"];
            if([meta isKindOfClass:[NSDictionary class]]){
                NSInteger code = [meta[@"code"] integerValue];
                if(code == 200){
                    completion(YES);
                }else completion(NO);
            }
            
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(NO);
        NSLog(@"error : %@",error);
    }];
    
}




- (void) getFollowsCompletion:(void(^)(NSMutableArray*))completion {
    
    NSMutableArray *follows = [NSMutableArray array];
    
    [self callAPIGetFollowsCompletion:^(id data){
        if([data isKindOfClass:[NSArray class]]){
            for(id subData in data){
                if([subData isKindOfClass:[NSDictionary class]]){
                    InstaUser *user = [[InstaUser alloc] initWithData:subData];
                    [follows addObject:user];
                }
            }
        }
        completion(follows);
    }];
}

- (void) getFollowedCompletion:(void(^)(NSMutableArray*))completion{
    NSMutableArray *followed = [NSMutableArray array];
    
    [self callAPIGetFollowsCompletion:^(id data){
        if([data isKindOfClass:[NSArray class]]){
            for(id subData in data){
                if([subData isKindOfClass:[NSDictionary class]]){
                    InstaUser *user = [[InstaUser alloc] initWithData:subData];
                    [followed addObject:user];
                }
            }
        }
        completion(followed);
    }];
    
}

- (void) getLikedCompletion:(void(^)(NSMutableArray*))completion{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"users/self/media/liked?access_token=%@",token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            completion(data);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
}

- (void) getMediaNearByDistance:(NSString*)distance completion:(void(^)(NSMutableArray*))completion{
    //https://api.instagram.com/v1/media/search?lat=48.858844&lng=2.294351&access_token=ACCESS-TO
    
    //21.028667
    //105.852148
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:INSTAGRAM_ACCESS_TOKEN];
    NSString *path = [NSString stringWithFormat:@"media/search?lat=21.028667&lng=105.852148&access_token=%@",token];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.instagram.com/v1/"]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]]){
            
            id data = [responseObject objectForKey:@"data"];
            completion(data);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(nil);
        NSLog(@"error : %@",error);
    }];
    
    
}


@end
