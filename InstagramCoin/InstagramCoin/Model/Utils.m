//
//  Utils.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "Utils.h"

static Utils *theUtils = nil;

@implementation Utils

+ (id) shareInstance{
    if(theUtils == nil){
        theUtils = [Utils new];
    }
    return theUtils;
}

- (void) downloadVideo:(NSString*)path completion:(void(^)(NSString* url))completion{
    
    NSString *videoId = [[path componentsSeparatedByString:@"/"] lastObject];
    NSString *fileAtPath = [[self doccumentDirectory] stringByAppendingPathComponent:videoId];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        if(completion != nil){
            completion(fileAtPath);
        }
        NSLog(@"downloaded !");
        return ;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"Downloading Started");
        NSURL  *url = [NSURL URLWithString:path];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {

            dispatch_async(dispatch_get_main_queue(), ^{
                [urlData writeToFile:fileAtPath atomically:YES];
                if(completion != nil){
                    completion(fileAtPath);
                }
                NSLog(@"dowload done");
                
            });
        }
        
    });
    
    
}

- (NSString*) getVideoPath:(NSString*)path{
    NSString *videoId = [[path componentsSeparatedByString:@"/"] lastObject];
    NSString *fileAtPath = [[self doccumentDirectory] stringByAppendingPathComponent:videoId];
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        return fileAtPath;
    }
    
    return nil;
}

- (NSString*) doccumentDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}





@end
