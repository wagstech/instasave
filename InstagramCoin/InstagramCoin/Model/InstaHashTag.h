//
//  InstaHashTag.h
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstaHashTag : NSObject

- (id) initWithData:(NSDictionary*)data;

@property (strong,nonatomic) NSString *hashTag;
@property (assign,nonatomic) NSInteger  media_count;

@end
