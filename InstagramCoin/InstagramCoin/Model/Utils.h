//
//  Utils.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (id) shareInstance;

- (void) downloadVideo:(NSString*)path completion:(void(^)(NSString* url))completion;
- (NSString*) getVideoPath:(NSString*)path;

@end
