//
//  InstaUser.m
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "InstaUser.h"

@implementation InstaUser

- (id) initWithData:(NSDictionary*)data{
    
    self.full_name = data[@"full_name"];
    self.userId = data[@"id"];
    self.profile_picture = data[@"profile_picture"];
    self.username = data[@"username"];
    self.bio = data[@"bio"];
    self.website = data[@"website"];
    
    id counts = data[@"counts"];
    if([counts isKindOfClass:[NSDictionary class]]){
        self.media = [counts[@"media"] integerValue];
        self.follows = [counts[@"follows"] integerValue];
        self.followed_by = [counts[@"followed_by"] integerValue];
    }
    
    return self;
}


@end
