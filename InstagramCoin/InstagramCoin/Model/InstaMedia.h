//
//  InstaMedia.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstaUser.h"


@interface InstaMedia : NSObject

- (id) initWithData:(NSDictionary*)data;

@property (strong,nonatomic) NSString *attribution;
@property (strong,nonatomic) NSDictionary *caption;
@property (strong,nonatomic) NSString *captionStr;
@property (strong,nonatomic) NSString *filter;
@property (strong,nonatomic) NSString *mediaId;

@property (strong,nonatomic) NSString *photo_low_resolution;
@property (strong,nonatomic) NSString *photo_standard_resolution;
@property (strong,nonatomic) NSString *photo_thumbnail;

@property (strong,nonatomic) NSString *video_low_bandwidth;
@property (strong,nonatomic) NSString *video_low_resolution;
@property (strong,nonatomic) NSString *video_standard_resolution;

@property (strong,nonatomic) NSString *link;
@property (strong,nonatomic) NSString *location;
@property (strong,nonatomic) NSArray *tags;
@property (strong,nonatomic) NSArray *users_in_photo;
@property (strong,nonatomic) NSString *type;
@property (strong,nonatomic) InstaUser *user;

@property (assign,nonatomic) NSInteger commentCount;
@property (assign,nonatomic) NSInteger likeCount;
@property (assign,nonatomic) NSInteger user_has_liked;
@property (assign,nonatomic) NSTimeInterval created_time;


@end
