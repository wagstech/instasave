//
//  InstaUser.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/11/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstaUser : NSObject

- (id) initWithData:(NSDictionary*)data;

@property (strong,nonatomic) NSString *full_name;
@property (strong,nonatomic) NSString *userId;
@property (strong,nonatomic) NSString *profile_picture;
@property (strong,nonatomic) NSString *username;

@property (strong,nonatomic) NSString *website;
@property (strong,nonatomic) NSString *bio;

@property (assign,nonatomic) NSInteger media;
@property (assign,nonatomic) NSInteger follows;
@property (assign,nonatomic) NSInteger followed_by;

@end
