//
//  ConstantHandle.h
//  InstagramCoin
//
//  Created by Dreamup on 2/10/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#ifndef ConstantHandle_h
#define ConstantHandle_h


//set User authentication and url
#define INSTAGRAM_AUTHURL @"https://api.instagram.com/oauth/authorize/"
#define INSTAGRAM_APIURl  @"https://api.instagram.com/v1/users/"
#define INSTAGRAM_CLIENT_ID @"f7e620ad2d7a48099f59c782af56a867"// 205f0bf8b50a4d0eaa71b7f35fbe7e9c
#define INSTAGRAM_CLIENTSERCRET @"b2850e7ce5aa4b2faaac8e35abd6be6a"// 7b4cb492a064457a892e8e93125feabf
#define INSTAGRAM_REDIRECT_URL  @"http://trams.co.kr"//https://www.facebook.com/Rango0o0o0
#define INSTAGRAM_ACCESS_TOKEN  @"access_token"
#define INSTAGRAM_SCOPE         @"likes+comments+relationships+basic+public_content+follower_list"

//Contant Url
#define ACCESS_TOKEN    @"#access_token="
#define UNSIGNED        @"UNSIGNED"
#define CODE            @"code="
#define END_POINT_URL   @"https://api.instagram.com/oauth/access_token"
#define HTTP_METHOD     @"POST"
#define CONTENT_LENGTH  @"Content-Length"
#define REQUEST_DATA    @"application/x-www-form-urlencoded"
#define CONTENT_TYPE    @"Content-Type"

//share Photo Constant
#define DOCUMENT_FILE_PATH @"Documents/originalImage.ig"
#define APP_URL   @"instagram://app"
#define UTI_URL   @"com.instagram.exclusivegram"
#define MESSAGE   @"Instagram not installed in this device!\nTo share image please install instagram."


#endif /* ConstantHandle_h */
