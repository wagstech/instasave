//
//  MyProfileCollectionCell.h
//  InstagramCoin
//
//  Created by Dreamup on 2/15/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarImv;

@property (weak, nonatomic) IBOutlet UILabel *postCountLb;

@property (weak, nonatomic) IBOutlet UILabel *followerCountLb;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLb;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLb;


@end
