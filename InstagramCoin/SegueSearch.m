//
//  SegueSearch.m
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "SegueSearch.h"

@implementation SegueSearch

- (void)perform{
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenH = [[UIScreen mainScreen] bounds].size.height;
    
    CGRect desFrame = CGRectMake(screenW, screenH - 120 , screenW, screenH - 120);
    
    destinationViewController.view.frame = desFrame;
    
    // Add the destination view as a subview, temporarily
    [sourceViewController.view addSubview:destinationViewController.view];
    [sourceViewController addChildViewController:destinationViewController];
    
    desFrame.origin.y =  60;
    desFrame.origin.x = 0;
    
    [UIView animateWithDuration:2.0 animations:^{
        destinationViewController.view.frame = desFrame;
    }];
    
    
    
}

@end
