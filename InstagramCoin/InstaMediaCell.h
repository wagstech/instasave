//
//  InstaMediaCell.h
//  InstagramCoin
//
//  Created by Dreamup on 2/13/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol InstanMediaCellDelegate <NSObject>

- (void) didSelectShare:(id)cell;
- (void) didSelectLike:(id)cell;

@end

@interface InstaMediaCell : UITableViewCell

@property (weak,nonatomic) id<InstanMediaCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *photoImv;
@property (weak, nonatomic) IBOutlet UIButton *likeBt;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;
@property (weak, nonatomic) IBOutlet UIView *containView;

@property (strong,nonatomic) AVPlayer *avPlayer;

- (IBAction)likeAcion:(id)sender;
- (IBAction)shareAction:(id)sender;


@end

