//
//  WatermarkView.h
//  InstagramCoin
//
//  Created by Nguyen Manh Tuan on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WatermarkView : UIView

@property (weak, nonatomic) IBOutlet UIView *blueView;

@property (weak, nonatomic) IBOutlet UIImageView *appIconImv;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImv;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@end
