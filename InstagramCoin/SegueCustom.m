//
//  SegueCustom.m
//  InstagramCoin
//
//  Created by Dreamup on 2/14/17.
//  Copyright © 2017 Dreamup. All rights reserved.
//

#import "SegueCustom.h"

@implementation SegueCustom

- (void)perform{
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenH = [[UIScreen mainScreen] bounds].size.height;
    
    CGRect desFrame = CGRectMake(screenW, 0, screenW, screenH - 60);
    
    destinationViewController.view.frame = desFrame;
    
    // Add the destination view as a subview, temporarily
    [sourceViewController.view addSubview:destinationViewController.view];
    [sourceViewController addChildViewController:destinationViewController];
    
    desFrame.origin.x =  0;
    
    [UIView animateWithDuration:0.3 animations:^{
        destinationViewController.view.frame = desFrame;
    }];
    
   
    
}


@end
